# CarCar

* Person 1 - Which microservice?
Zeke worked on the Sales microservice.
* Person 2 - Which microservice?
Alyn worked on the Service microservice.
## Design
This project is called CarCar which creates and manages Inventory for a car dealership, it's Service Center and it's Sales. The design of this project is to create RESTful API calls to those 3 different microservices: Inventory, Sales, and Service.

The front-end of the application uses React, the back-end of the application uses Django, and the database used is PostgreSQL.
## Instructions to run the application
This application runs by building Docker containers and Docker images. Please have Docker Desktop downloaded in order to continue on with the instructions below:

#### Cloning the Repository
* In a terminal, change directories to one that you can clone this project into.
* Type this command into your terminal: `git clone https://gitlab.com/ejsamatua/project-beta.git`
* Switch directories to this project directory.

#### Starting the application with Docker
* After switching to the project's directory in your terminal, type the following commands once and hit enter after each one:

* `docker volume create beta-data`
* `docker-compose build`
* `docker-compose up`

## Navigation with URLs
* The server uses `http://localhost:3000` as the Home Page. When entering that into your browser, you will see a navigation bar. Below is a breakdown of each URL and what feature it links to:

#### Vehicle Models:

| Feature          | URL          |
|:-----------------|:-------------|
|List vehicle models|http://localhost:3000/models|
|Create vehicle models| http://localhost:3000/models/new|

#### Automobiles:

| Feature          | URL          |
|:-----------------|:-------------|
|List automobiles| http://localhost:3000/automobiles/all|
|Create automobiles| http://localhost:3000/automobiles/new|

#### Manufacturers:

| Feature          | URL          |
|:-----------------|:-------------|
|List manufactureres|http://localhost:3000/manufacturers|
|Add a manufactureres|http://localhost:3000/manufacturers/new|

#### Service Microservice:

| Feature          | URL          |
|:-----------------|:-------------|
|List service appointments| http://localhost:3000/appointments/list|
|Create service appointments| http://localhost:3000/appointments/new|
|List service history based VIN| http://localhost:3000/services/new|
|Create a technicion| http://localhost:3000/technicians/new|

#### Sales Microservice:

| Feature          | URL          |
|:-----------------|:-------------|
|Add a sales record|http://localhost:3000/salesrecords/new/|
|Add a customer|http://localhost:3000/customers/new/|
|Add a sales person|http://localhost:3000/salesperson/new|
|List of all sales|http://localhost:3000/salesrecords/|
|List Sales by sales person from drop down selection|http://localhost:3000/salesperson/history/|

## Context Map Diagrams
* Below is a diagram of the back-end and front-end of the project and how they connect:
![Diagram using ExcaliDraw](/img/Project%20Beta%20Context%20Map.png)

## Sales microservice - Ezekiel Samatua

For the Sales microservice, I had created models that include AutomobileVO, SalesPeson, Customer, and SalesRecord. Since Automobiles data was in the microservice of Inventory, the creation of a value object called Automobile was necessary in order to grab that existing data. To grab this data, I implemented a function called 'poll' in order to grab the data.

In the back-end for Sales Microservice under the views.py (path is sales/api/sales_rest/views.py) I have implemented view functions for each model to ensure that the back-end data is being created and stored properly. To check this was correct, I utilized Insomnia to perform my RESTful API calls.

To ensure proper creation, the Sales Record model would need a creation of an Automobile from the Inventory service first, Sales Person and Customer added based off the properties of the models, and then entering a price for the sale. Please see below for the ports and URLs to make these API calls:

#### Sales Microservice RESTful API calls:

| Feature          | Method          | URL          |
|:-----------------|:----------------|:-------------|
|List Sales Record| GET |http://localhost:8090/api/salesrecords/|
|Show Sales Record Details| GET |http://localhost:8090/api/salesrecords/|
|Create Sales Record| POST |http://localhost:8090/api/salesrecords/|
|List Customers| GET |http://localhost:8090/api/customers/|
|Show Customer Details| GET |http://localhost:8090/api/customers/enterIDNumber|
|Create a Customer| POST |http://localhost:8090/api/customers/|
|List Sales People| GET |http://localhost:8090/api/salespersons/|
|Show Sales Person Details| GET |http://localhost:8090/api/salespersons/enterIDNumber|
|Create a Sales Person| POST |http://localhost:8090/api/salespersons/|


## Service microservice - Alyn Liang

For the service microservies, I created AutomobileVO, Technician, and Appointment as my models. Then I added a foreign key into my Appointment model to get access to my Technician model. I created a poller that created objects for me by polling from the inventory API.

In the back-end for Services Microservice under the views.py (path is service/api/service_rest/views.py) I first created functions for each model to ensure that the back-end data is being stored and created properly. After, I went into the Urls.py, which was empty at the time, and created paths for the models in my views.py. I used insomnia to test if the paths I had set up were working properly.

#### Services Microservice RESTful API calls:

| Feature          | Method          | URL          |
|:-----------------|:----------------|:-------------|
|Create Technician| POST |http://localhost:8080/api/technicians/|
|Create Service appointment| POST |http://localhost:8080/api/appointments/|
|Create Service appointment| GET |http://localhost:8080/api/appointments/|
|List Technicians| GET |http://localhost:8080/api/technicians/|
|List Appointments| GET |http://localhost:8080/api/appointments/|
|View Appointments| GET |http://localhost:8080/api/appointments/|
|View Service History| POST |http://localhost:8080/api/appointments/|
