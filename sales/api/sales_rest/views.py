from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, SalesPerson, Customer, SalesRecord
from .encoders import (
    AutomobileVOEncoder,
    SalesPersonEncoder,
    CustomerEncoder,
    SalesRecordEncoder,
)

# Create your views here.

# API view functions for list of sales people, creating sales person, and showing their details

@require_http_methods(["GET", "POST"])
def api_list_sales_persons(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_persons = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_persons,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create Sales Person."}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET"])
def api_show_sales_person(request, pk):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_persons,
            encoder=SalesPersonEncoder,
            safe=False,
        )

# API view functions to list customers, create a customer, show customer details

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a customer."}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET"])
def api_show_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

# API for listing sales records, creating sales record, and showing sales record details

@require_http_methods(["GET", "POST"])
def api_list_sales_record(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesRecordEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            if automobile.available is True:


                content["automobile"] = automobile

                customer = Customer.objects.get(name=content["customer"])
                content["customer"] = customer

                sales_person = SalesPerson.objects.get(name=content["sales_person"])
                content["sales_person"] = sales_person

                automobile.available = False
                automobile.save()

                record = SalesRecord.objects.create(**content)
                return JsonResponse(
                    record,
                    encoder=SalesRecordEncoder,
                    safe=False,
                )
            else:
                response = JsonResponse(
                    {"message": "Car is no longer available."}
                )
                response.status_code = 400
                return response
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Cannot create sales record."},
                status=404,
            )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_sales_record(request, pk):
    if request.method == "GET":
        try:
            sales_persons = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                sales_persons,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Does no exist"})
            response.status_code = 404
            return response
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            content = json.loads(request.body)
            salesrecord = SalesRecord.objects.get(id=pk)

            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content["automobile"] = automobile

            customer_name = content["customer"]
            customer = Customer.objects.get(name=customer_name)
            content["customer"] = customer

            sales_person = content["sales_person"]
            sales_person = SalesPerson.objects.get(name=sales_person)
            content["sales_person"] = sales_person

            props = ["sales_person", "customer", "automobile", "price"]
            for prop in props:
                if prop in content:
                    setattr(salesrecord, prop, content[prop])
            salesrecord.save()
            return JsonResponse(
                salesrecord,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    # Delete request method for a Sales Record
    else:
        count, _ = SalesRecord.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})




# ensures Poller is working properly by displaying it within insomnia
@require_http_methods(["GET"])
def api_list_automobiles(request):
    if request.method == "GET":
        cars = AutomobileVO.objects.all()
        return JsonResponse(
            cars,
            encoder=AutomobileVOEncoder,
            safe=False,
        )
