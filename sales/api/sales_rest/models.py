from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, default=True)
    available = models.BooleanField(default=True)

    def __str__(self):
        return self.vin

class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name

class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class SalesRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name='sales',
        on_delete=models.PROTECT,
    )

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name='sales',
        on_delete=models.PROTECT,
    )

    customer = models.ForeignKey(
        Customer,
        related_name='sales',
        on_delete=models.PROTECT,
    )

    price = models.CharField(max_length=200)
