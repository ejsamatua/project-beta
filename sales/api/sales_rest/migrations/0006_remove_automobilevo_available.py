# Generated by Django 4.0.3 on 2023-01-24 04:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0005_automobilevo_import_href'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='automobilevo',
            name='available',
        ),
    ]
