from django.urls import path
from .views import (
    api_list_sales_persons,
    api_show_sales_person,
    api_list_customers,
    api_show_customer,
    api_list_sales_record,
    api_list_automobiles,
    api_show_sales_record,
)

urlpatterns = [
    # URL endpoints for salespersons and specific sales person
    path("salespersons/", api_list_sales_persons, name="api_list_sales_persons"),
    path("salespersons/<int:pk>/", api_show_sales_person, name="api_show_sales_person"),

    #URL endpoints and specific customer for customers
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:pk>/", api_show_customer, name="api_show_custoemr"),

    #URL endpoints for salesrecords and specific sales record
    path("salesrecords/", api_list_sales_record, name="api_list_sales_records"),
    path("salesrecords/<int:pk>/", api_show_sales_record, name="api_show_sales_records"),

    #URL endpoint for automobile VO to ensure polling is correctly working
    path("automobiles/", api_list_automobiles, name="api_list_automobile"),
]
