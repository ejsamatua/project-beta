import React, {useEffect, useState} from 'react';

function AutoMobileList({automobiles}) {
console.log(automobiles)
    if (automobiles === undefined) {
        return null;
    }
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Model</th>
              <th>Color</th>
              <th>Year</th>
              <th>VIN</th>
            </tr>
          </thead>
          <tbody>
          {automobiles.map(auto => {
            return (
              <tr key={auto.id}>
                <td>{auto.model.name}</td>
                <td>{auto.color}</td>
                <td>{auto.year}</td>
                <td>{auto.vin}</td>
              </tr>
            );
          })}
          </tbody>
        </table>
      );
    }

export default AutoMobileList;
