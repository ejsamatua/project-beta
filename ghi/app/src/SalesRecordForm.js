import React, {useEffect, useState} from 'react';

function SalesRecordForm(props) {

// This hanldes the submission of the form

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.automobile = automobile;
        data.sales_person = salesPerson;
        data.customer = customer;
        data.price = price

        const salesRecordUrl = 'http://localhost:8090/api/salesrecords/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(salesRecordUrl, fetchConfig);
        if (response.ok) {
          const newSalesRecord = await response.json();
          console.log(newSalesRecord);

          setAutomobile('');
          setSalesPerson('');
          setCustomer('');
          setPrice('');
          fetchAutomobiles('');
          props.getSales();
        }
    }

// Sets our array destructioning for our setter and variables



  const [automobile, setAutomobile] = useState('');
  const handleAutoMobileChange = (event) => {
    const value = event.target.value;
    setAutomobile(value);
  }

  const [salesPerson, setSalesPerson] = useState('');
  const handleSalesPerson = (event) => {
    const value = event.target.value;
    setSalesPerson(value);
  }

  const [customer, setCustomer] = useState('');
  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const [price, setPrice] = useState('');
  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  }

// Sets our states for the drop down menu

  // Fetching the autombiles data
  const [automobiles, setAutomobiles] = useState([]);
  const fetchAutomobiles = async () => {

    const autoMobileUrl = 'http://localhost:8090/api/automobiles/';

    const response = await fetch(autoMobileUrl);

    if (response.ok) {
      const autoMobileData = await response.json();
      setAutomobiles(autoMobileData)
    }
  }

  // Fetching the sales people data
  const [salespersons, setSalesPersons] = useState([]);
  const fetchSalesPersons = async () => {

    const salesPersonsUrl = 'http://localhost:8090/api/salespersons/';

    const response = await fetch(salesPersonsUrl);

    if (response.ok) {
      const salesPersonsData = await response.json();
      setSalesPersons(salesPersonsData.sales_persons)
    }
  }

  // Fetching the customer data
  const [customers, setCusomters] = useState([]);
  const fetchCustomers = async () => {

    const customersUrl = 'http://localhost:8090/api/customers/';

    const response = await fetch(customersUrl);

    if (response.ok) {
      const customersData = await response.json();
      setCusomters(customersData.customer)
    }
  }


// Use the fectched data with the use effect hook
  useEffect(() => {
    fetchAutomobiles();
    fetchSalesPersons();
    fetchCustomers();
  }, []);

// JSX for React

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Record a new sale!</h1>
        <form onSubmit={handleSubmit}>
        <div className="mb-3">
            <select onChange={handleAutoMobileChange} value={automobile} required name="automobile"  className="form-select">
                <option value="">Choose an automobile</option>
                {automobiles.map(automobile => {
                   if( automobile.available === true) {
                    return (
                        <option key={automobile.import_href} value={automobile.import_href}>
                        {automobile.vin}
                        </option>
                    );
                }})}
            </select>
          </div>
          <div className="mb-3">
            <select onChange={handleSalesPerson} value={salesPerson} required name="sales_person" className="form-select">
                <option value="">Choose a sales person</option>
                {salespersons.map(salesPerson => {
                    return (
                        <option key={salesPerson.id} value={salesPerson.name}>
                        {salesPerson.name}
                        </option>
                    );
                })}
            </select>
          </div>
          <div className="mb-3">
            <select onChange={handleCustomerChange} value={customer} required name="customer" className="form-select">
                <option value="">Choose a customer</option>
                {customers.map(customer => {
                    return (
                        <option key={customer.id} value={customer.name}>
                        {customer.name}
                        </option>
                    );
                })}
            </select>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handlePriceChange} value={price} placeholder="Price" required type="text" name="price" className="form-control" />
            <label htmlFor="price">Price (enter with $ sign)</label>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  );
}

export default SalesRecordForm;
