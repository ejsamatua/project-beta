import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Z-S Autos</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>

      {/* Invetory Navigation Links */}
      <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
        <ul className="navbar-nav">
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Inventory
            </a>
            <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new/">Add a Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/">Manufacturer List</NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/models/new">Add a Vehicle Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/">Vehicle Models List</NavLink>
            </li>

            <li className="nav-item">
                  <NavLink className="nav-link" to="/automobiles/new">Add an Automobile</NavLink>
            </li>
            <li className="nav-item">
                  <NavLink className="nav-link" to="/automobiles/all">Automobiles List</NavLink>
            </li>
            </ul>
          </li>
        </ul>
      </div>

      {/* Sales Microservice Navigation Links */}
      <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
      <ul className="navbar-nav">
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Sales Navigation
          </a>
          <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">

            <li className="nav-item">
                <NavLink className="nav-link" to="/customers/new/">Add a Customer</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/salesperson/new/">Add a Sales Person</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/salesrecords/new/">Add a Sales Record</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/salesrecords/">Sales Records List</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/salesperson/history/">Sales Person History</NavLink>
              </li>
          </ul>
        </li>
      </ul>
    </div>

    {/* Service Microservice Navigation Links */}
    <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
      <ul className="navbar-nav">
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Service Navigation
          </a>
          <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">

          <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/new">Make a Service Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/list">Appointment List</NavLink>
            </li>
            <li className="nav-item">
                  <NavLink className="nav-link" to="/technicians/new">New technician</NavLink>
            </li>
            <li className="nav-item">
                  <NavLink className="nav-link" to="/services/new">Service History</NavLink>
            </li>
          </ul>
        </li>
      </ul>
    </div>



          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
