import React, {useEffect, useState} from 'react';

function SalesPersonForm() {

//this is handling the submit of the form

  const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {};
      data.name = name;
      data.employee_number = employeeNumber;

      console.log(data);

      const salesPersonUrl = 'http://localhost:8090/api/salespersons/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(salesPersonUrl, fetchConfig);
      if (response.ok) {
        const newSalesPerson = await response.json();
        console.log(newSalesPerson);

        setName('');
        setEmployeeNumber('');
        fetchSalesPersons();
      }
  }

// Sets our array destructioning for our setter and variables

  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const [employeeNumber, setEmployeeNumber] = useState('');
  const handleEmployeeNumberChange = (event) => {
    const value = event.target.value;
    setEmployeeNumber(value);
  }

// Fetching the sales people data to update the Sales People list
  const [salespersons, setSalesPersons] = useState([]);
  const fetchSalesPersons = async () => {

    const salesPersonsUrl = 'http://localhost:8090/api/salespersons/';

    const response = await fetch(salesPersonsUrl);

    if (response.ok) {
      const salesPersonsData = await response.json();
      setSalesPersons(salesPersonsData.sales_persons)
    }
  }

 // Use the fectched data with the use effect hook
  useEffect(() => {
    fetchSalesPersons();
  }, []);


// JSX for React

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Add a new Sales Person!</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" className="form-control" />
            <label htmlFor="name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleEmployeeNumberChange} value={employeeNumber} placeholder="Employee Number" required type="text" name="employee_number" className="form-control" />
            <label htmlFor="employee_number">Employee Number</label>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  );
}

export default SalesPersonForm;
