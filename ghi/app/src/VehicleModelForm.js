import React, {useEffect, useState} from 'react';

function VehicleModelForm(props) {

// This hanldes the submission of the form

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = vehicleName;
        data.picture_url = picture;
        data.manufacturer_id = manufacturer;

        console.log(data);

        const vehicleModelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(vehicleModelUrl, fetchConfig);
        if (response.ok) {
          const newVehicleModel = await response.json();
          console.log(newVehicleModel);

          setVehicleName('');
          setPicture('');
          setManufacturer('');
          props.getModels();

        }
    }

// Sets our array destructioning for our setter and variables

  const [vehicleName, setVehicleName] = useState('');
  const handleVehicleName = (event) => {
    const value = event.target.value;
    setVehicleName(value);
  }

  const [picture, setPicture] = useState('');
  const handlePicture = (event) => {
    const value = event.target.value;
    setPicture(value);
  }

  const [manufacturer, setManufacturer] = useState('');
  const handleManufacturer = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

// Sets our states for the drop down menu


  const [manufacturers, setManufacturers] = useState([]);
  const fetchManufacturers = async () => {

    const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(manufacturersUrl);

    if (response.ok) {
      const manufacturersData = await response.json();
      setManufacturers(manufacturersData.manufacturers)
    }
  }

// Use the fectched data with the use effect hook
  useEffect(() => {
    fetchManufacturers();
  }, []);

// JSX for React

return (
    <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a vehicle model </h1>
                <form onSubmit={handleSubmit} id="Create-a-vehicle-model ">
                <div className="form-floating mb-3">
                    <input onChange={handleVehicleName} placeholder="Name" required type="text" name="name" className="form-control" value={vehicleName} />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePicture} placeholder="Picture URL" required type="text" name="picture_url " className="form-control" value={picture} />
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                <select onChange={handleManufacturer} required name="manufacturer" className="form-select" value={manufacturer}>
                <option value="">Choose a Manufacturer</option>
                    {manufacturers.map(manufacturer =>{
                    return (
                            <option key={manufacturer.id} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                            )
                     })}
                </select>
                </div>
                <button className="btn btn-success btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    </div>
)
}


export default VehicleModelForm;
