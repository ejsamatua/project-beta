import React, { useState } from 'react';

function ServiceHistory({appointments, setAppointment}) {
    const [searchVin, setSearchVin] = useState('')
    const [apt, setApt] = useState(appointments);
    const handleSearch = async (event) => {
        if (searchVin.length > 0) {
            event.preventDefault();
            console.log(searchVin, "This is the VIN");
        }
        const filteredAppointments = appointments.filter((appointment) =>
            appointment.vin.includes(searchVin)
        );
        console.log(appointments, "this is the VIN")
        setApt(filteredAppointments);
    }
    const handleInputChange = async (event) => {
        const value = event.target.value;
        setSearchVin(value);
    };
    if (appointments === undefined) {
      return null;
    }
    return (
        <>
            <form onSubmit={handleSearch} className="input-group">
                <input onChange={handleInputChange} type="search" className="form-control rounded" placeholder="Search VIN" aria-label="Search" aria-describedby="search-addon" />
                <button type="submit" className="btn btn-outline-secondary">Search</button>
            </form>
            <div className=' text-center'>
                <h1 className='mb-3'>Service History</h1>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr className='text-center'>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Service Date</th>
                        <th>Reason</th>
                        <th>Technician</th>
                        <th>VIP</th>
                    </tr>
                </thead>
                <tbody>
                    {apt.map(appointment => {
                        return (
                            <tr key={appointment.id} className='text-center'>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.customer_name }</td>
                                <td>{ appointment.starts}</td>
                                <td>{ appointment.reason }</td>
                                <td>{ appointment.technician.employee_name }</td>
                                <td>{ String(appointment.vip_treatment) }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default ServiceHistory;
