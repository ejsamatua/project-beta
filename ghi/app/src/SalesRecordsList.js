
function SalesRecordsList({sales}) {

    if (sales === undefined) {
        return null;
    }
    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Sales Person</th>
                <th>Employee Number</th>
                <th>Customer's Name</th>
                <th>Automobile VIN</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            {sales.map(sale => {
                return (
                <tr key={sale.id}>
                    <td>{ sale.sales_person.name }</td>
                    <td>{ sale.sales_person.employee_number }</td>
                    <td>{ sale.customer.name }</td>
                    <td>{ sale.automobile.vin }</td>
                    <td>{ sale.price }</td>

                </tr>
                );
            })}

            </tbody>
        </table>
    );
}


export default SalesRecordsList;
