import React, {useEffect, useState} from 'react';

function AutomobileForm(props) {

// This hanldes the submission of the form

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;


        console.log(data);

        const automobilesUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(automobilesUrl, fetchConfig);
        if (response.ok) {
          const newAutomobile = await response.json();
          console.log(newAutomobile);

          setColor('');
          setYear('');
          setVin('');
          setModel('');
          props.getAutomobiles();
        }
    }

// Sets our array destructioning for our setter and variables

  const [vin, setVin] = useState('');
  const handleVin = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  const [color, setColor] = useState('');
  const handleColor = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const [year, setYear] = useState('');
  const handleYear = (event) => {
    const value = event.target.value;
    setYear(value);
  }

  const [model, setModel] = useState('');
  const handleModel = (event) => {
    const value = event.target.value;
    setModel(value);
  }


// Sets our states for the drop down menu


  const [models, setModels] = useState([]);
  const fetchModels = async () => {

    const modelsUrl = 'http://localhost:8100/api/models/';

    const response = await fetch(modelsUrl);

    if (response.ok) {
      const modelsData = await response.json();
      setModels(modelsData.models)
    }
  }

// Use the fectched data with the use effect hook
  useEffect(() => {
    fetchModels();
  }, []);

// JSX for React

return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create an Automobile</h1>
                <form onSubmit={handleSubmit} id="create-appointment-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleVin} value={vin} placeholder="vin" required type="text" name="vin" className="form-control" />
                        <label htmlFor="vin"> Vehicle VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleColor} value={color} placeholder="Color" required type="text" name="color" className="form-control" />
                        <label htmlFor="color">Vehicle Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleYear} value={year} placeholder="year" required type="number" name="year"  className="form-control" />
                        <label htmlFor="year">Vehicle Year</label>
                    </div>
                    <select onChange={handleModel} required name="model" className="form-select" value={model} >
                <option value="">Model</option>

                {models.map(model => {
                      return (
                          <option key={model.id} value={model.id}>
                              {model.name}
                          </option>
                      );
                        })}
                    </select>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
);
}

export default AutomobileForm;
