import React from 'react';
import {
  MDBCarousel,
  MDBCarouselItem,
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBCardImage,
  MDBBtn,
  MDBRipple,
  MDBContainer, MDBRow, MDBCol
} from 'mdb-react-ui-kit';

import { NavLink } from 'react-router-dom';

function MainPage() {
  return (
  <>
    <div className="px-4 py-5 my-5 text-center">
    <h1 className="display-5 fw-bold">Z-S Autos</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
    </div>
    <div>
    <MDBCarousel showControls fade>
      <MDBCarouselItem
        height='mx-auto'
        widht='mx-auto'
        className='w-100 d-block'
        itemId={1}
        src='https://automobiles.honda.com/-/media/Honda-Automobiles/Vehicles/2023/Civic-Sedan/Hero/my23-civic-sedan-hero-mobile-A-2x.jpg%202x'
        alt='Honda Civic'
      />
      <MDBCarouselItem
        height='mx-auto'
        widht='mx-auto'
        className='w-100 d-block'
        itemId={2}
        src='https://tesla-cdn.thron.com/delivery/public/image/tesla/97f565b2-b81d-43e7-95f9-a16a570bf44b/bvlatuR/std/0x0/p100d-announcement'
        alt='...'
      />
      <MDBCarouselItem
        height='mx-auto'
        widht='mx-auto'
        className='w-100 d-block'
        itemId={3}
        src='https://imageio.forbes.com/specials-images/imageserve/63750667d79fa4c6b7f59b8f/2023-Toyota-Prius-hybrid-electric-car/0x0.jpg?format=jpg&crop=2195,1463,x21,y284,safe&width=960'
        alt='...'
      />
    </MDBCarousel>
    </div>

    {/* Card Templates */}
    <div className='pt-5' />
    <MDBContainer className='pt-5'>
      <MDBRow>
        <MDBCol size='md'>
          <MDBCard>
            <MDBRipple rippleColor='light' rippleTag='div' className='bg-image hover-overlay'>
              <MDBCardImage src='https://thumbs.dreamstime.com/b/cars-sale-stock-lot-row-cars-sale-stock-lot-row-car-dealer-inventory-170311695.jpg' fluid alt='...' />
              <a>
                <div className='mask' style={{ backgroundColor: 'rgba(251, 251, 251, 0.15)' }}></div>
              </a>
            </MDBRipple>
            <MDBCardBody>
              <MDBCardTitle>Inventory</MDBCardTitle>
              <MDBCardText>
                Add a manufacturer to your invetory, vehicle models, and automobile information. Includes a list view for each category.
              </MDBCardText>
              <div className="offcanvas offcanvas-end" id="demo">
                <div className="offcanvas-header">
                  <h1 className="offcanvas-title">Inventory</h1>
                  <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas"></button>
                </div>
                <div class="offcanvas-body">
                  <p>Click one of the navigation links below:</p>
                  <NavLink className="nav-link" to="/manufacturers/new/"><button className="btn btn-dark">Create a Manufacturer</button></NavLink>
                  <NavLink className="nav-link" to="/manufacturers/"><button className="btn btn-dark">Manufacturer List</button></NavLink>
                  <NavLink className="nav-link" to="/models/new"><button className="btn btn-dark">Add a Vehicle Model</button></NavLink>
                  <NavLink className="nav-link" to="/models/"><button className="btn btn-dark">Vehicle Models List</button></NavLink>
                  <NavLink className="nav-link" to="/automobiles/new"><button className="btn btn-dark">Add an Automobile</button></NavLink>
                  <NavLink className="nav-link" to="/automobiles/all"><button className="btn btn-dark">Automobiles List</button></NavLink>
                </div>
              </div>
              <button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#demo">
                Inventory Navigation
              </button>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>

        <MDBCol size='md'>
          <MDBCard>
              <MDBRipple rippleColor='light' rippleTag='div' className='bg-image hover-overlay'>
                <MDBCardImage src='https://d9s1543upwp3n.cloudfront.net/wp-content/uploads/2021/05/car-sales.jpeg' fluid alt='...' />
                <a>
                  <div className='mask' style={{ backgroundColor: 'rgba(251, 251, 251, 0.15)' }}></div>
                </a>
              </MDBRipple>
              <MDBCardBody>
                <MDBCardTitle>Sales</MDBCardTitle>
                <MDBCardText>
                  Manage your customers and Sales People information while having the ability to record new sales and view sales in bulk or by sales person.
                </MDBCardText>
                <div className="offcanvas offcanvas-start" id="demo2">
                  <div className="offcanvas-header">
                    <h1 className="offcanvas-title">Sales</h1>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"></button>
                  </div>
                  <div class="offcanvas-body">
                    <p>Click one of the navigation links below:</p>
                    <NavLink className="nav-link" to="/customers/new/"><button className="btn btn-dark">Add a Customer</button></NavLink>
                    <NavLink className="nav-link" to="/salesperson/new/"><button className="btn btn-dark">Add a Sales Person</button></NavLink>
                    <NavLink className="nav-link" to="/salesrecords/new/"><button className="btn btn-dark">Add a Sales Record</button></NavLink>
                    <NavLink className="nav-link" to="/salesrecords/"><button className="btn btn-dark">Sales Record List</button></NavLink>
                    <NavLink className="nav-link" to="/salesperson/history/"><button className="btn btn-dark">Sales Record List</button></NavLink>
                  </div>
              </div>
                <button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#demo2">
                  Sales Navigation
                </button>
              </MDBCardBody>
            </MDBCard>
        </MDBCol>

        <MDBCol size='md'>
        <MDBCard>
            <MDBRipple rippleColor='light' rippleTag='div' className='bg-image hover-overlay'>
              <MDBCardImage src='https://t4.ftcdn.net/jpg/02/21/15/71/360_F_221157116_fHHySD3mVT1CSDrDEswKshjZhi3ty1nT.jpg' fluid alt='...' />
              <a>
                <div className='mask' style={{ backgroundColor: 'rgba(251, 251, 251, 0.15)' }}></div>
              </a>
            </MDBRipple>
            <MDBCardBody>
              <MDBCardTitle>Services</MDBCardTitle>
              <MDBCardText>
                Schedule a service apointment, and create a Technician for these appointsment. View your appointments in bulk or by VIN number.
              </MDBCardText>
              <div class="offcanvas offcanvas-end" id="demo3">
                <div class="offcanvas-header">
                  <h1 class="offcanvas-title">Services</h1>
                  <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas"></button>
                </div>
                <div class="offcanvas-body">
                  <p>Click one of the navigation links below:</p>
                  <NavLink className="nav-link" to="/appointments/new"><button className="btn btn-dark">Make a Service Appointment</button></NavLink>
                  <NavLink className="nav-link" to="/appointments/list"><button className="btn btn-dark">List of Appointments</button></NavLink>
                  <NavLink className="nav-link" to="/technicians/new"><button className="btn btn-dark">Add a Technician</button></NavLink>
                  <NavLink className="nav-link" to="/services/new"><button className="btn btn-dark">Make a Service Appointment</button></NavLink>
              </div>
              </div>
                <button className="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#demo3">
                Services Navigation
                </button>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
    <div className='pt-5' />
    <div className='pt-5' />

  </>

  );
}

export default MainPage;
