import { BrowserRouter, Routes, Route } from 'react-router-dom';
// Imports for components
import MainPage from './MainPage';

// Sales Microservice imports
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SalesRecordsList from './SalesRecordsList';
import SalesRecordForm from './SalesRecordForm';
import SalesPersonHistoryList from './SalesPersonHistoryList'
import Nav from './Nav';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import TechnicianForm from './TechnicianForm';
import ServiceHistory from './ServiceHistory';

// Inventory imports
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelList from './VehicleModelList';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import VehicleModelForm from './VehicleModelForm';

// Import for hooks
import { useState, useEffect } from 'react';

function App() {

  // Get all sales records
  const [sales, setSales] = useState([]);
  const getSales = async () => {
    const saleUrl = "http://localhost:8090/api/salesrecords/"
    const response = await fetch(saleUrl)

    if (response.ok) {
      const saleData = await response.json()
      const sales = saleData.sales
      setSales(sales);
    }
  }

// Get all Manufacturer names
  const [manufacturers, setManufacturers] = useState([]);
  const getManufacturers = async () => {
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/"
    const response = await fetch(manufacturerUrl)

    if (response.ok) {
      const manufacturerData = await response.json()
      const manufacturers = manufacturerData.manufacturers
      setManufacturers(manufacturers);
    }
  }

  const [models, setModels] = useState([]);
  const getModels = async () => {
    const modelsUrl = "http://localhost:8100/api/models/"
    const response = await fetch(modelsUrl)

    if (response.ok) {
      const modelData = await response.json()
      const models = modelData.models
      setModels(models);
    }
  }

  const [appointments, setAppointments] = useState([]);
  const getAppointments = async () => {
    const appointmentUrl = "http://localhost:8080/api/appointments/"
    const response = await fetch(appointmentUrl)

    if (response.ok) {
      const appointmentData = await response.json()
      const appointments = appointmentData.appointment

      setAppointments(appointments);
    }
  }

  const [automobiles, setAutomobiles] = useState([]);
  const getAutomobiles = async () => {
    const automobilesUrl = "http://localhost:8100/api/automobiles/"
    const response = await fetch(automobilesUrl)

    if (response.ok) {
      const automobileData = await response.json()
      const automobiles = automobileData.autos
      setAutomobiles(automobiles);
    }
  }


  useEffect(() => {
    getSales();
    getManufacturers();
    getModels();
    getAppointments();
    getAutomobiles();

  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          {/* Main Page Route */}
          <Route path="/" element={<MainPage />} />

          {/* Routes for Sales Person*/}
          <Route path="salesperson">
            <Route path='new' element={<SalesPersonForm />} />
            <Route path='history' element={<SalesPersonHistoryList sales={sales} />} />
          </Route>

          {/* Routes for Customers */}
          <Route path="customers">
            <Route path='new' element={<CustomerForm />} />
          </Route>

          {/* Routes for SalesRecords */}
          <Route path="salesrecords" element={<SalesRecordsList sales={sales}/>} />
          <Route path="salesrecords">
            <Route path='new' element={<SalesRecordForm getSales={getSales}/>} />
          </Route>

          {/* Routes for Manufacturers */}
          <Route path="manufacturers" element={<ManufacturerList manufacturers={manufacturers} />} />
          <Route path="manufacturers">
            <Route path='new' element={<ManufacturerForm getManufacturers={getManufacturers}/>} />
          </Route>

          {/* Routes for Vehicle Models */}
          <Route path="models" element={<VehicleModelList models={models} />} />
          <Route path="models/new" element={<VehicleModelForm getModels={getModels} />} />

          {/* Routes for Appointments Models */}
          <Route path="appointments/list" element={<AppointmentList appointments={appointments} />} />
          <Route path="appointments">
            <Route path="new" element={<AppointmentForm />} />
          </Route>

          {/* Routes for technicians Models */}
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />} />
          </Route>

          {/* Routes for technicians service*/}
          <Route path="services" >
            <Route path="new" element={<ServiceHistory appointments={appointments}/>}/>
          </Route>

          <Route path="automobiles">
            <Route path= "new" element={<AutomobileForm getAutomobiles={getAutomobiles} />} />
            <Route path="all" element={<AutomobileList automobiles={automobiles} />}/>
          </Route>


        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
