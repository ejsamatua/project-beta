import React, {useState} from 'react';

function ManufacturerForm(props) {

//this is handling the submit of the form

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;


        console.log(data);

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
          const newManufacturer = await response.json();
          console.log(newManufacturer);

          setName('');
          props.getManufacturers();
        }
    }

// Sets our array destructioning for our setter and variables

  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }


// JSX for React

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Add a new Manufacturer!</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" className="form-control" />
            <label htmlFor="name">Name</label>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  );
}

export default ManufacturerForm;
