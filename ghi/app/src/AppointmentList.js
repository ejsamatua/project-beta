import React, {useEffect, useState} from 'react';

function AppointmentList({appointments}) {


    if (appointments === undefined) {
        return null;
    }
    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Vin</th>
                <th>Customer Name</th>
                <th>Date</th>
                <th>Reason</th>
                <th>Technician</th>
            </tr>
            </thead>
            <tbody>
            {appointments.map(appointment => {
                return (
                <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    <td>{ appointment.customer_name }</td>
                    <td>{ appointment.starts }</td>
                    <td>{ appointment.reason }</td>
                    <td>{ appointment.technician.employee_name }</td>

                </tr>
                );
            })}

            </tbody>
        </table>
    );
}

export default AppointmentList;
