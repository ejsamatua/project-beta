import React, {useEffect, useState} from 'react';

function TechnicianForm() {

//this is handling the submit of the form

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.employee_name = employeeName;
        data.employee_id = employeeNumber;

        console.log(data);

        const techniciansUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(techniciansUrl, fetchConfig);
        if (response.ok) {
          const newTechnician = await response.json();
          console.log(newTechnician);

          setEmployeeName('');
          setEmployeeNumber('');
        }
    }

// Sets our array destructioning for our setter and variables

  const [employeeName, setEmployeeName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setEmployeeName(value);
  }

  const [employeeNumber, setEmployeeNumber] = useState('');
  const handleEmployeeNumberChange = (event) => {
    const value = event.target.value;
    setEmployeeNumber(value);
  }

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Enter a new technician</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input onChange={handleNameChange} value={employeeName} placeholder="Employee Name" required type="text" name="employee_name" className="form-control" />
            <label htmlFor="employee_name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleEmployeeNumberChange} value={employeeNumber} placeholder="Employee Number" required type="text" name="employee_number" className="form-control" />
            <label htmlFor="employee_number">Employee Number</label>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  );
}

export default TechnicianForm;
