import React, {useEffect, useState} from 'react';

function SalesPersonHistoryList({sales}) {

    const [salesPerson, setSalesPerson] = useState('');
    const handleSalesPerson = (event) => {
      const value = event.target.value;
      setSalesPerson(value);
    }

    const [salespersons, setSalesPersons] = useState([]);
    const fetchSalesPersons = async () => {

      const salesPersonsUrl = 'http://localhost:8090/api/salespersons/';

      const response = await fetch(salesPersonsUrl);

      if (response.ok) {
        const salesPersonsData = await response.json();
        setSalesPersons(salesPersonsData.sales_persons)
      }
    }

    useEffect(() => {
        fetchSalesPersons();
      }, []);

    if (sales === undefined) {
        return null;
    }
    return (
        <>
        <h1>Sales Person History</h1>
        <div className="mb-3">
            <select onChange={handleSalesPerson} value={salesPerson} required name="sales_person" className="form-select">
                <option value="">Choose a sales person</option>
                {salespersons.map(salesPerson => {
                    return (
                        <option key={salesPerson.id} value={salesPerson.name}>
                        {salesPerson.name}
                        </option>
                    );
                })}
            </select>
          </div>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Sales Person</th>
                    <th>Employee Number</th>
                    <th>Customer's Name</th>
                    <th>Automobile VIN</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                {sales.filter(sale => sale.sales_person.name === salesPerson)
                .map(sale => {
                return (
                    <tr key={sale.automobile.import_href}>
                        <td>{sale.sales_person.name}</td>
                        <td>{sale.sales_person.employee_number}</td>
                        <td>{sale.customer.name}</td>
                        <td>{sale.automobile.vin}</td>
                        <td>{sale.price}</td>
                    </tr>
                    );
                })}
            </tbody>
            </table>
        </>
        );

}


export default SalesPersonHistoryList;
