import React, {useEffect, useState} from 'react';

function CustomerForm() {

//this is handling the submit of the form

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.address = address;
        data.phone_number = phoneNumber

        console.log(data);

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
          const newCustomer = await response.json();
          console.log(newCustomer);

          setName('');
          setAddress('');
          setPhoneNumber('');
          fetchCustomers();
        }
    }

// Sets our array destructioning for our setter and variables

  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const [address, setAddress] = useState('');
  const handleAddressChange = (event) => {
    const value = event.target.value;
    setAddress(value);
  }

  const [phoneNumber, setPhoneNumber] = useState('');
  const handlePhoneNumberChange = (event) => {
    const value = event.target.value;
    setPhoneNumber(value);
  }

    // Fetching the customer data to update the list of customers
  const [customers, setCusomters] = useState([]);
  const fetchCustomers = async () => {

    const customersUrl = 'http://localhost:8090/api/customers/';

    const response = await fetch(customersUrl);

    if (response.ok) {
      const customersData = await response.json();
      setCusomters(customersData.customer)
    }
  }

  // Use the fectched data with the use effect hook
  useEffect(() => {
    fetchCustomers();
  }, []);


// JSX for React

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Add a new Customer!</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" className="form-control" />
            <label htmlFor="name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleAddressChange} value={address} placeholder="Address" required type="text" name="address" className="form-control" />
            <label htmlFor="address">Address</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handlePhoneNumberChange} value={phoneNumber} placeholder="Phone Number" required type="text" name="phone_number" className="form-control" />
            <label htmlFor="phone_number">Phone Number</label>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  );
}

export default CustomerForm;
