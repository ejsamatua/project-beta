
function VehicleModelList({models}) {

    if (models === undefined) {
        return null;
    }
    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Model Name</th>
                <th>Car Picture</th>
                <th>Manufacturer</th>
            </tr>
            </thead>
            <tbody>
            {models.map(model => {
                return (
                <tr key={model.id}>
                    <td>{ model.name }</td>
                    <td>
                        <img
                            src={model.picture_url}
                            alt=""
                            width="250"
                            height="200"
                            />
                    </td>
                    <td>{ model.manufacturer.name }</td>

                </tr>
                );
            })}

            </tbody>
        </table>
    );
}


export default VehicleModelList;
