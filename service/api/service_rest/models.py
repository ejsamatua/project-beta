from django.db import models
from django.urls import reverse
# Create your models here.

class AutomobileVO(models.Model):
    color = models.CharField(max_length=200)
    year = models.PositiveIntegerField()
    vin = models.CharField(max_length=20, unique=True)

class Technician(models.Model):
    employee_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.id})

    def __str__(self):
        return f"{self.employee_name} | {self.employee_id}"

class Appointment(models.Model):
    vin = models.CharField(max_length=200)
    customer_name = models.CharField(max_length=200)
    starts = models.DateTimeField(null=True)
    reason = models.TextField()
    technician = models.ForeignKey(
        Technician,
        related_name = "techician",
        on_delete = models.PROTECT
    )
    vip_treatment = models.BooleanField(default=False)
    finishedOrCanceled = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.id})
