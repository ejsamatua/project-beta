from django.shortcuts import render
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment, AutomobileVO
from common.json import ModelEncoder

# Create your views here.

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "employee_id",
        "employee_name",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "color",
        "year"
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "starts",
        "reason",
        "technician",
        "vip_treatment",
        "finishedOrCanceled",
        ]
    encoders = {"technician": TechnicianEncoder(),
    "automobile": AutomobileVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe = False
            )
        except:
            response = JsonResponse(
                {"message": "could not create the technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_technician(request, pk):
    if request.method == "DELETE":
        technician = Technician.objects.get(id=pk)
        technician.delete()
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )

    elif request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.filter.get(id=pk)
        return (JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        ))

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(employee_name=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee_name"},
                status=400,
            )
        auto_vin = content['vin']
        cars = AutomobileVO.objects.all()
        auto_vins = []
        for car in cars:
            auto_vins.append(car.vin)
        if auto_vin in auto_vins:
            content['vip_treatment'] = True
        appointments = Appointment.objects.create(**content)
        return JsonResponse(appointments,
        encoder=AppointmentEncoder,
        safe=False,
        )


@require_http_methods(["DELETE", "PUT", "GET"])
def api_detail_appointment(request, pk):
    if request.method == "DELETE":
        appointment = Appointment.objects.get(id=pk)
        appointment.delete()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    elif request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

    else:
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False
        )


@require_http_methods(["GET"])
def api_show_appointment(request, vin):
    if request.method == "GET":
        automobile = AutomobileVO.objects.get(vin=vin)
        appointments = Appointment.objects.filter(automobile=automobile)
        return JsonResponse(
            {"appointments": appointments},
            encoder = AppointmentEncoder,
            safe=False,
        )

@require_http_methods(['GET'])
def api_service_history(request, vin):

    appointments = Appointment.objects.filter(vin=vin)
    return JsonResponse(
        {"appointments": appointments},
        encoder=AppointmentEncoder,
    )
